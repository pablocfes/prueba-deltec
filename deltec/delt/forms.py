from select import select
from django import forms
from .models import Usuario, Recurso
from django.forms import ModelForm

class UserForm(ModelForm):
    class Meta:
        model = Usuario
        fields = 'nombres', 'apellidos', 'identificacion', 
        widgets = {
            'nombres': forms.TextInput(attrs={'placeholder': 'Ingrese nombres'}),
            'apellidos': forms.TextInput(attrs={'placeholder': 'Ingrese apellidos'}),
            'identificacion': forms.NumberInput(attrs={'placeholder': 'Ingrese identificacion'}),
            
        }

class RecursoForm(ModelForm):
    class Meta:
        model = Recurso
        fields =  'usuario','categoria', 'codigo', 'nombre', 'marca', 'serie' 
        
        widgets = {
            
            'categoria': forms.TextInput(attrs={'placeholder': 'Ingrese categoria'}),
            'codigo': forms.TextInput(attrs={'placeholder': 'Ingrese codigo'}),
            'nombre': forms.TextInput(attrs={'placeholder': 'ingrese nombre'}),
            'marca': forms.TextInput(attrs={'placeholder': 'Ingrese marca'}),
            'serie': forms.TextInput(attrs={'placeholder': 'ingrese serie'}),
            
        }