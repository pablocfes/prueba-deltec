from django.db import models

# Create your models here.


class Usuario(models.Model):
    nombres = models.CharField(max_length=250)
    apellidos = models.CharField(max_length=250)
    identificacion = models.IntegerField()
    created = models.DateTimeField(auto_now_add=True, null=True)
    

    def __str__(self) -> str:
        return self.nombres

class Recurso(models.Model):
    usuario = models.ForeignKey(Usuario,  on_delete=models.SET_NULL,  blank=True, null=True)
    categoria = models.CharField(max_length=250)
    codigo = models.CharField(max_length=250)
    nombre = models.CharField(max_length=250)
    marca = models.CharField(max_length=250)
    serie = models.CharField(max_length=250)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-updated', '-created']

    def __str__(self) -> str:
        return self.nombre


class Historial(models.Model):
    usuario = models.CharField(max_length=255)
    recurso = models.CharField(max_length=255)
    created = models.DateTimeField(auto_now_add=True)


    class Meta:
        ordering = [ '-created']

    def __str__(self) -> str:
        return self.usuario