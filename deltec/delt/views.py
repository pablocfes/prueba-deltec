from django.shortcuts import render, redirect

from django.views.generic import FormView, TemplateView, UpdateView, DeleteView
from .models import Recurso, Usuario, Historial
from .forms import UserForm, RecursoForm
# Create your views here.



class Inicio(TemplateView): #Simple template del inicio
    template_name = 'inicio.html'
    
class TotalPersonasRecursos(TemplateView): #Lista de personas Registradas que cuentan con recursos
    template_name = 'personarecursos.html'

    def get_context_data(self,*args, **kwargs):
        context = super(TotalPersonasRecursos, self).get_context_data(*args,**kwargs)
        context['recursos'] = Recurso.objects.filter(usuario__isnull=False)
        return context
        
class HistorialTotal(TemplateView): #Lista de personas Registradas que cuentan con recursos
    template_name = 'historial.html'

    def get_context_data(self,*args, **kwargs):
        context = super(HistorialTotal, self).get_context_data(*args,**kwargs)
        context['historial'] = Historial.objects.all()
        return context       

class TotalPersonas(TemplateView): #Total de personas registradas
    template_name = 'personatotal.html'

    def get_context_data(self,*args, **kwargs):
        context = super(TotalPersonas, self).get_context_data(*args,**kwargs)
        context['users'] = Usuario.objects.all()
        context['recursos'] = Recurso.objects.all()
        return context

class PersonasUpdate(UpdateView): #view para editar persona
    model = Usuario
    template_name= 'registro.html'
    fields = ['nombres','apellidos','identificacion']
    success_url= "/persona/total"

class PersonasDelete(DeleteView): #view para eliminar persona
    model = Usuario
    success_url= "/persona/total"
    template_name = 'confirmDelete.html'

class TotalRecursos(TemplateView): #Total de recursos registrados
    template_name = 'recursototal.html'
    
    def get_context_data(self,*args, **kwargs):
        context = super(TotalRecursos, self).get_context_data(*args,**kwargs)
        context['recursos'] = Recurso.objects.all()
        
        return context

class RecursosUpdate(UpdateView): #view para editar recurso
    model = Recurso
    template_name= 'recurso.html'
    fields = ['usuario','categoria','codigo','nombre', 'marca','serie']
    success_url= "/recurso/total"

    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
       
        try:
           
            data = self.get_form().save()
    
            usuario = request.POST.get('usuario')
            us = Usuario.objects.get(id=usuario)
            recurso = request.POST.get('nombre')

            
            historial = Historial.objects.create(usuario=us, recurso=recurso)

            return redirect('totalrecurso')
            
        except Exception as e:
            data['error'] = str(e)

class RecursosDelete(DeleteView): #view para eliminar recurso
    model = Recurso
    success_url= "/recurso/total"
    template_name = 'confirmDelete.html'
   

class RegistrarPersona(FormView): #Template para registrar personas
    template_name = 'registro.html'
    form_class = UserForm

    def get_context_data(self, *args, **kwargs):
        form = UserForm()
        context = super().get_context_data(*args,**kwargs)
        context['form'] = form
        return context
    
    def post(self, request, *args, **kwargs):
        data = {}
        try:
            form = UserForm(request.POST)
            if form.is_valid(): #Vemos si el form enviado es valido, y lo procesamos
                nombres = request.POST.get('nombres')
                apellidos = request.POST.get('apellidos')
                id = request.POST.get('identificacion')

                new = Usuario.objects.create(nombres=nombres, apellidos=apellidos,identificacion=id)
                return redirect('totalpersona')
                
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)




class RegistrarRecurso(FormView): #Template para registrar recurso
    template_name = 'recurso.html'
    form_class = RecursoForm

    def get_context_data(self, *args, **kwargs):
        form = RecursoForm()
        context = super().get_context_data(*args,**kwargs)
        context['form'] = form
        return context
    
    def post(self, request, *args, **kwargs):
        data = {}
        try:
            form = RecursoForm(request.POST)
            
            if form.is_valid(): #Vemos si el form enviado es valido, y lo procesamos
                
                new = Recurso()
                usuario = request.POST.get('usuario') 
                if usuario: #Si hay usuario para asignar lo tomamos y guardamos
                    us = Usuario.objects.get(id=usuario)
                    new.usuario = us
                    
                new.categoria = request.POST.get('categoria')
                new.codigo = request.POST.get('codigo')
                new.nombre = request.POST.get('nombre')
                new.marca = request.POST.get('marca')
                new.serie = request.POST.get('serie')
                new.save() #Guardamos la informacion suministrada
                if usuario:
                    historial = Historial.objects.create(usuario=us, recurso=new)
                return redirect('totalrecurso')
                

                
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)