from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('', views.Inicio.as_view(), name='inicio'),
    path('persona/', views.RegistrarPersona.as_view(), name='persona'),
    path('persona/<pk>/', views.PersonasUpdate.as_view(), name='persona-edit'),
    path('persona/eliminar/<pk>/', views.PersonasDelete.as_view(), name='persona-eliminar'),

    path('persona/recursos', views.TotalPersonasRecursos.as_view(), name='personarecursos'),
    path('persona/total', views.TotalPersonas.as_view(), name='totalpersona'),


    path('recurso/', views.RegistrarRecurso.as_view(), name='recurso'),
    path('recurso/<pk>/', views.RecursosUpdate.as_view(), name='recurso-edit'),
    path('recurso/eliminar/<pk>/', views.RecursosDelete.as_view(), name='persona-eliminar'),

    path('recurso/total', views.TotalRecursos.as_view(), name='totalrecurso'),
    path('historial/', views.HistorialTotal.as_view(), name='historial'),

    
    
    

]